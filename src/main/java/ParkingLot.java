import java.text.ParseException;
import java.util.*;

public class ParkingLot {

    private final int capacity;
    ArrayList<Object> parkedVehicles = new ArrayList<>();

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    private boolean isSpaceAvailable() {
        return parkedVehicles.size() < capacity;
    }

    public String park(Object car) throws  ParkingLotException {
        if (isSpaceAvailable()) {
            parkedVehicles.add(car);
            return ParkingStatus.PARKED.toString();
        }
        throw new ParkingLotException(ParkingStatus.NOT_PARKED.toString());
    }

    public String unPark(Object car) throws ParkingLotException {
        if (parkedVehicles.contains(car)) {
            parkedVehicles.remove(car);
            return ParkingStatus.UNPARKED.toString();
        }
        throw new ParkingLotException(ParkingStatus.NOT_AVAILABLE_TO_UNPARK.toString());
    }
}

