public enum ParkingStatus {
    PARKED("your car is parked successfully!!"),
    NOT_PARKED("your car is not parked"),
    UNPARKED("your car is unParked successfully!!"),
    NOT_AVAILABLE_TO_UNPARK("your car to be unParked is not present in the parking lot");

    private final String message;

    ParkingStatus(String message) {
        this.message = message;
    }
}
