public class ParkingLotException extends Throwable {
    public ParkingLotException(String message) {
        super(message);
    }
}
