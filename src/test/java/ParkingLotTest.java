import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParkingLotTest {
    @Test
    void shouldReturnYourCarIsParkedSuccessfullyWhenTheCarIsParked() throws ParkingLotException {
        ParkingLot parkingLot = new ParkingLot(5);
        Object car = new Object();

        String expected = ParkingStatus.PARKED.toString();

        assertEquals(expected, parkingLot.park(car));
    }

    @Test
    void shouldThrowParkingLotExceptionMessageWhenTheCarIsNotParked() throws ParkingLotException {
        ParkingLot parkingLot = new ParkingLot(1);
        Object car = new Object();
        parkingLot.park(car);

        ParkingLotException exception = assertThrows(ParkingLotException.class, () -> parkingLot.park(car));

        assertEquals(ParkingStatus.NOT_PARKED.toString(), exception.getMessage());
    }

    @Test
    void shouldReturnYourCarIsUnParkedSuccessfullyMessageWhenTheCarIsUnParked() throws ParkingLotException{
        ParkingLot parkingLot = new ParkingLot(1);
        Object car = new Object();

        parkingLot.park(car);
        String expected = ParkingStatus.UNPARKED.toString();

        assertEquals(expected, parkingLot.unPark(car));
    }

    @Test
    void shouldThrowParkingLotExceptionMessageWhenTheCarToBeUnParkIsNotParkedInTheParkingLot() {
        ParkingLot parkingLot = new ParkingLot(1);
        Object car = new Object();

        ParkingLotException exception = assertThrows(ParkingLotException.class, () -> parkingLot.unPark(car));

        assertEquals(ParkingStatus.NOT_AVAILABLE_TO_UNPARK.toString(), exception.getMessage());

    }


}